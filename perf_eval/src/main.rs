use clap::{Parser, ValueEnum};

use fec::decode;
use proofs::aplonk_pc::AplonkCommitment;
use proofs::aplonk_pc::AplonkVerifierKey;
use proofs::aplonk_pc::SetupParams;
use proofs::kzg_multipoly::KzgMultiCommitment;
use proofs::semi_avid_pr::SemiAvidPrCommitment;
use proofs::{field, PolynomialCommitmentScheme};

use rand::rngs::SmallRng;
use rand::RngCore;
use rand::SeedableRng;

use ark_bls12_381::Bls12_381;
use ark_bn254::Bn254;
use ark_ec::pairing::Pairing;
use ark_ec::AffineRepr;
use ark_ff::PrimeField;
use ark_poly::univariate::DensePolynomial;
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::kzg10::Powers;
use ark_poly_commit::kzg10::VerifierKey;
use ark_std::ops::Div;

use rs_merkle::algorithms::Sha256;
use rs_merkle::Hasher;

use reed_solomon_erasure::galois_prime::Field as GF;

use std::fmt::Display;
use std::fmt::Formatter;
use std::time::Duration;
use std::time::Instant;

type UniPolyBLS381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;
type UniPolyBN254 = DensePolynomial<<Bn254 as Pairing>::ScalarField>;

#[derive(Parser)]
#[clap(after_help = "EXAMPLES:\n    \
    (in Nushell) prove 496 bytes with aPlonK, read the JSON and convert to durations\n    \
    > perf_eval --size 496 -k 8 -n 10 -m aplonk     \n        \
        | from json\n        \
        | into duration commit proof verification decoding\n    \
    ────────────┬───────────────\n    \
    nb_bytes    │496\n    \
    k           │8\n    \
    n           │10\n    \
    method      │Semi-AVID-PR\n    \
    commit      │3ms 954µs 648ns\n    \
    proof       │15µs 128ns\n    \
    verification│1ms 69µs 513ns\n    \
    decoding    │649µs 119ns\n    \
    ────────────┴──────────────-")]
struct Cli {
    #[arg(default_value_t = 1234, help = "the seed of the pseudo-RNG")]
    seed: u64,
    #[arg(
        short,
        long,
        value_name = "size",
        help = "the size of the random data to generate, i.e. the number of bytes that will be proven"
    )]
    size: usize,
    #[arg(short, help = "the number of source shards to split the data into")]
    k: usize,
    #[arg(
        short,
        help = "the number of encoded shards to generate, linear combinations of the source ones"
    )]
    n: usize,
    #[arg(short, long, value_enum, default_value_t = Curve::BLS12_381, help = "the elliptic curve to use")]
    curve: Curve,
    #[arg(short, long, value_enum, default_value_t = Method::Aplonk, help = "the proving system to use")]
    method: Method,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Curve {
    BLS12_381,
    BN254,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Method {
    Aplonk,
    KZG,
    SemiAvid,
}

impl Display for Method {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::KZG => write!(f, "KZG+"),
            Self::SemiAvid => write!(f, "Semi-AVID-PR"),
            Self::Aplonk => write!(f, "aPlonK"),
        }
    }
}

fn timeit<F: FnOnce() -> T, T>(f: F) -> (T, Duration) {
    let start = Instant::now();
    let result = f();
    (result, start.elapsed())
}

fn setup<E, P>(
    degree: usize,
    nb_polynomials: usize,
) -> (SetupParams<E>, Powers<'static, E>, AplonkVerifierKey<E>)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let params = proofs::aplonk_pc::setup::<E, P>(degree, nb_polynomials).expect("setup failed");

    let (powers, verifier_key) = proofs::trim(params.kzg.clone(), degree);
    let verifier_key = AplonkVerifierKey {
        vk_psi: verifier_key,
        tau_1: params.ipa.tau_1,
        g1: params.kzg.powers_of_g[0].into_group(),
        g2: params.kzg.h.into_group(),
    };

    (params, powers, verifier_key)
}

fn run_semi_avid_pr<E, P>(
    powers: Powers<E>,
    data: ([u8; 32], usize, Vec<P>),
    points: &[E::ScalarField],
) -> (Vec<fec::Shard>, Duration, Duration, Duration)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let (commit, commit_time) = timeit(|| {
        SemiAvidPrCommitment::<E, P>::commit(data.clone(), Some(powers.clone()))
            .expect("commit failed")
    });

    let (blocks, proof_time) = timeit(|| {
        SemiAvidPrCommitment::<E, P>::prove(
            commit.clone(),
            data.clone(),
            Some(points),
            Some(powers.clone()),
        )
        .expect("prove failed")
    });

    let block = blocks.get(0).unwrap();
    let (_, verification_time) = timeit(|| {
        SemiAvidPrCommitment::<E, P>::verify(block, Some(powers.clone()))
            .expect("verification failed")
    });

    (
        blocks.into_iter().map(|x| x.shard).collect::<Vec<_>>(),
        commit_time,
        proof_time,
        verification_time,
    )
}

fn run_kzg_mp<E, P>(
    powers: Powers<E>,
    verifier_key: VerifierKey<E>,
    data: ([u8; 32], usize, Vec<P>),
    points: &[E::ScalarField],
) -> (Vec<fec::Shard>, Duration, Duration, Duration)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let (commit, commit_time) = timeit(|| {
        KzgMultiCommitment::<E, P>::commit(data.clone(), Some(powers.clone()))
            .expect("commit failed")
    });

    let (blocks, proof_time) = timeit(|| {
        KzgMultiCommitment::<E, P>::prove(
            commit.clone(),
            data.clone(),
            Some(points),
            Some(powers.clone()),
        )
        .expect("prove failed")
    });

    let block = blocks.get(0).unwrap();
    let (_, verification_time) = timeit(|| {
        KzgMultiCommitment::<E, P>::verify(block, Some(verifier_key.clone()))
            .expect("verification failed")
    });

    (
        blocks.into_iter().map(|x| x.shard).collect::<Vec<_>>(),
        commit_time,
        proof_time,
        verification_time,
    )
}

fn run_aplonk<E, P>(
    params: SetupParams<E>,
    verifier_key: AplonkVerifierKey<E>,
    data: ([u8; 32], usize, Vec<P>),
    points: &[E::ScalarField],
) -> (Vec<fec::Shard>, Duration, Duration, Duration)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let (commit, commit_time) = timeit(|| {
        AplonkCommitment::<E, P>::commit(data.clone(), Some(params.clone())).expect("commit failed")
    });

    let (blocks, proof_time) = timeit(|| {
        AplonkCommitment::<E, P>::prove(
            commit.clone(),
            data.clone(),
            Some(points),
            Some(params.clone()),
        )
        .expect("prove failed")
    });

    let block = blocks.get(0).unwrap();
    let (_, verification_time) = timeit(|| {
        AplonkCommitment::<E, P>::verify(block, Some(verifier_key.clone()))
            .expect("verification failed")
    });

    (
        blocks.into_iter().map(|x| x.shard).collect::<Vec<_>>(),
        commit_time,
        proof_time,
        verification_time,
    )
}

fn run<E, P>(bytes: &[u8], k: usize, n: usize, method: Method)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let modulus_byte_size = <E as Pairing>::ScalarField::MODULUS_BIT_SIZE as usize / 8;
    let nb_polynomials = bytes.len() / modulus_byte_size / k;

    let degree = k.max(nb_polynomials);

    let (params, powers, verifier_key) = setup::<E, P>(degree, nb_polynomials);

    let elements = field::split_data_into_field_elements::<E>(bytes, k);
    let nb_polynomials = elements.len() / k;
    let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

    let data = (Sha256::hash(bytes), bytes.len(), polynomials);
    let points: Vec<<E as Pairing>::ScalarField> = (0..n)
        .map(|i| <E as Pairing>::ScalarField::from_le_bytes_mod_order(&[i as u8]))
        .collect();

    let (shards, commit_time, proof_time, verification_time) = match method {
        Method::Aplonk => run_aplonk::<E, P>(params, verifier_key, data, &points),
        Method::KZG => run_kzg_mp::<E, P>(powers, verifier_key.vk_psi, data, &points),
        Method::SemiAvid => run_semi_avid_pr::<E, P>(powers, data, &points),
    };

    let (_, decoding_time) = timeit(|| decode::<GF>(shards));

    println!(
        r#"{{"nb_bytes": {}, "k": {}, "n": {}, "method": "{}", "commit": {}, "proof": {}, "verification": {}, "decoding": {}}}"#,
        bytes.len(),
        k,
        n,
        method,
        commit_time.as_nanos(),
        proof_time.as_nanos(),
        verification_time.as_nanos(),
        decoding_time.as_nanos()
    )
}

fn main() {
    let cli = Cli::parse();

    let (k, n) = (cli.k, cli.n);

    let mut bytes = vec![0; cli.size];
    SmallRng::seed_from_u64(cli.seed).fill_bytes(bytes.as_mut_slice());

    match cli.curve {
        Curve::BLS12_381 => run::<Bls12_381, UniPolyBLS381>(&bytes, k, n, cli.method),
        Curve::BN254 => run::<Bn254, UniPolyBN254>(&bytes, k, n, cli.method),
    }
}
