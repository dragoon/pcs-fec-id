# setup the dev environment
export def setup [] {
    ^git config --local core.hooksPath .githooks
}

# format the whole source base at once
export def fmt [--verbose] {
    if $verbose {
        ^cargo fmt --all
    } else {
        ^cargo fmt --all --verbose -- --verbose
    }
}

# check the entire workspace for compilation errors
export def check [] {
    ^cargo check --workspace --all-targets
}

# check the entire workspace for things that will make *clippy* sad
export def clippy [] {
    ^cargo clippy --workspace --all-targets -- -D warnings -A clippy::len-without-is-empty
}

# run the tests over the whole workspace
export def test [] {
    ^cargo test --workspace
}
