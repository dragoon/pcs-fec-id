# Performance Evaluation of Polynomial Commitments for Erasure Code Based Information Dispersal
Erasure coding is a common tool that improves the dependability of distributed
storage systems. Basically, to decode data that has been encoded from $k$ source
shards into $n$ output shards with an erasure code, a node of the network must
download at least $k$ shards and launch the decoding process.
However, if one of the shards is intentionally or accidentally modified, the
decoding process will reconstruct invalid data. To allow the verification of
each shard independently without running the decoding for the whole data, the
encoder can add a cryptographic proof to each output shard which certifies its
validity.
In this paper, we focus on the following commitment-based schemes: **KZG+**,
**aPlonK-PC** and **Semi-AVID-PC**.
These schemes perform polynomial evaluations in the same way as a Reed-Solomon
encoding process.
Still, such commitment-based schemes may introduce huge computation times as
well as large storage space needs.
This paper compares their performance to help designers of distributed storage
systems identify the optimal proof depending on constraints like data size,
information dispersal and frequency of proof verification against proof
generation. We show that in most cases **Semi-AVID-PC** is the optimal solution,
except when the input files and the required amount of verifications  are large,
where **aPlonK-PC** is optimal.

> :bulb: **Note**  
> the paper can be found [here](https://gitlab.isae-supaero.fr/dragoon/pcs-fec-id/-/blob/paper-pdf/paper.pdf)
> or can be built by following the instructions in [this section](#build-the-paper-toc)

# Table of content
- [_The structure of the source base_](#the-structure-of-the-source-base-toc)
  - [_the `fec/` directory_](#the-fec-directory-toc)
  - [_the `src/` directory_](#the-src-directory-toc)
  - [_the `perf_eval/` directory_](#the-perf_eval-directory-toc)
  - [_the `benches/` directory_](#the-benches-directory-toc)
- [_Run the tests_](#run-the-tests-toc)
- [_Run the benchmarks_](#run-the-benchmarks-toc)
- [_Build the paper_](#build-the-paper-toc)

## The structure of the source base [[toc](#table-of-content)]
### the `fec/` directory [[toc](#table-of-content)]
This is a helper crate to perform the decoding stage.

:point_right: please have a look at `cargo doc --package fec --open` for more information.

### the `src/` directory [[toc](#table-of-content)]
This is the main crate of the study, called `proofs` internally.
It defines the three proof schemes studied in the paper and a common interface to use them outside
this project.

:point_right: please have a look at `cargo doc --package proofs --open` for more information.

### the `perf_eval/` directory [[toc](#table-of-content)]
A little tool to prove random bytes and measure times on the fly. `perf_eval` can prove useful
when one wants to measure the time it takes for a particular set of parameters to prove and verify
a piece of random data, without multiple iterations.

:point_right: please have a look at `cargo run --package perf_eval -- --help` for more information.

### the `benches/` directory [[toc](#table-of-content)]
This is the directory that allows one to run the benchmarks and reproduce the results of the paper.

:point_right: please see [_Run the benchmarks_](#run-the-benchmarks) below for more information

## Run the tests [[toc](#table-of-content)]
```shell
cargo test --workspace
```

## Run the benchmarks [[toc](#table-of-content)]
- install `cargo criterion`
```shell
cargo install cargo-criterion
```
- run the benchmarks, e.g. in Nushell
```shell
cargo criterion --output-format verbose --message-format json --package benches out> results.ndjson
```
:point_right: this will create a `./results.ndjson` file

## Build the paper [[toc](#table-of-content)]
> :bulb: **Note**
>
> there is also an extension of this paper available on the [`paper-extension` branch](https://gitlab.isae-supaero.fr/dragoon/pcs-fec-id/-/tree/paper-extension/paper?ref_type=heads)

- download the figures at [this address](https://gitlab.isae-supaero.fr/dragoon/pcs-fec-id/-/raw/paper-resources/images.zip)
- unzip the `.eps` files into `paper/images/`, e.g. with
```shell
unzip ~/downloads/images.zip -d .
```
- on Ubuntu, install the `texlive-full` package
- run, e.g. in Nushell
```shell
do {
    cd paper/
    latexmk -pdf main.tex
}
```
:point_right: this will create a `./paper/main.pdf` file
