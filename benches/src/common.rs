use rand::Rng;

pub const BENCH_CASES: [(usize, usize, usize); 11] = [
    (496, 4, 8),
    (2 * 496, 4, 8),
    (4 * 496, 4, 8),
    (8 * 496, 4, 8),
    (16 * 496, 4, 8),
    (32 * 496, 4, 8),
    (64 * 496, 4, 8),
    (128 * 496, 4, 8),
    (256 * 496, 4, 8),
    (512 * 496, 4, 8),
    (1024 * 496, 4, 8),
];

pub fn random_bytes(n: usize) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    (0..n).map(|_| rng.gen::<u8>()).collect()
}
