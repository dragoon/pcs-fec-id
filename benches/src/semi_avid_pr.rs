mod common;
use common::random_bytes;

use criterion::{criterion_group, criterion_main, Criterion};

use proofs::semi_avid_pr::SemiAvidPrCommitment;
use proofs::{field, trim, PolynomialCommitmentScheme};

use crate::common::BENCH_CASES;
use ark_bls12_381::Bls12_381;
use ark_bn254::Bn254;
use ark_ec::pairing::Pairing;
use ark_ff::PrimeField;
use ark_poly::univariate::DensePolynomial;
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::kzg10::KZG10;
use ark_std::ops::Div;
use ark_std::test_rng;
use rs_merkle::algorithms::Sha256;
use rs_merkle::Hasher;

fn semi_avid_pr_template<E, P>(c: &mut Criterion, name: &str)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut group = c.benchmark_group(name);

    for (nb_bytes, k, n) in BENCH_CASES {
        let bench_name = format!(r#"{{"nb_bytes": {nb_bytes}, "k": {k}, "n": {n}}}"#);

        let bytes = random_bytes(nb_bytes);

        let setup_degree = nb_bytes / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        let rng = &mut test_rng();
        let params = KZG10::<E, P>::setup(setup_degree, false, rng).expect("setup failed");
        let (powers, _) = trim(params, setup_degree);

        let elements = field::split_data_into_field_elements::<E>(&bytes, k);
        let nb_polynomials = elements.len() / k;
        let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

        let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
        let points: Vec<E::ScalarField> = (0..n)
            .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        group.bench_function(&format!("commit|{bench_name}"), |b| {
            b.iter(|| {
                SemiAvidPrCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();
            })
        });

        let commit =
            SemiAvidPrCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();

        group.bench_function(&format!("prove|{bench_name}"), |b| {
            b.iter(|| {
                SemiAvidPrCommitment::<E, P>::prove(
                    commit.clone(),
                    data.clone(),
                    Some(&points),
                    Some(powers.clone()),
                )
                .expect("prove failed")
            })
        });

        let blocks =
            SemiAvidPrCommitment::<E, P>::prove(commit, data, Some(&points), Some(powers.clone()))
                .expect("prove failed");

        group.bench_function(&format!("batch_verify|{bench_name}"), |b| {
            b.iter(|| {
                SemiAvidPrCommitment::<E, P>::batch_verify(blocks.as_slice(), Some(powers.clone()))
                    .unwrap()
            })
        });

        group.bench_function(&format!("verify_one|{bench_name}"), |b| {
            b.iter(|| {
                let block = blocks.get(0).unwrap();
                SemiAvidPrCommitment::<E, P>::verify(block, Some(powers.clone())).unwrap();
            })
        });

        group.bench_function(&format!("verify_all|{bench_name}"), |b| {
            b.iter(|| {
                for block in &blocks {
                    SemiAvidPrCommitment::<E, P>::verify(block, Some(powers.clone())).unwrap();
                }
            })
        });
    }

    group.finish();
}

fn semi_avid_pr_bls12(c: &mut Criterion) {
    semi_avid_pr_template::<Bls12_381, DensePolynomial<<Bls12_381 as Pairing>::ScalarField>>(
        c,
        "Semi-AVID-PR Proof System (BLS-12-381)",
    );
}

fn semi_avid_pr_bn254(c: &mut Criterion) {
    semi_avid_pr_template::<Bn254, DensePolynomial<<Bn254 as Pairing>::ScalarField>>(
        c,
        "Semi-AVID-PR Proof System (BN-254)",
    );
}

criterion_group!(benches, semi_avid_pr_bls12, semi_avid_pr_bn254);
criterion_main!(benches);
