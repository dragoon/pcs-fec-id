mod common;
use common::random_bytes;

use criterion::{criterion_group, criterion_main, Criterion};

use proofs::aplonk_pc::{setup, AplonkCommitment, AplonkVerifierKey};
use proofs::{field, trim, PolynomialCommitmentScheme};

use crate::common::BENCH_CASES;
use ark_bls12_381::Bls12_381;
use ark_bn254::Bn254;
use ark_ec::pairing::Pairing;
use ark_ec::AffineRepr;
use ark_ff::PrimeField;
use ark_poly::univariate::DensePolynomial;
use ark_poly::DenseUVPolynomial;
use ark_std::ops::Div;
use rs_merkle::algorithms::Sha256;
use rs_merkle::Hasher;

fn aplonk_template<E, P>(c: &mut Criterion, name: &str)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let modulus_byte_size = E::ScalarField::MODULUS_BIT_SIZE as usize / 8;

    let mut group = c.benchmark_group(name);
    group.sample_size(10);
    for (nb_bytes, k, n) in BENCH_CASES {
        let bench_name = format!(r#"{{"nb_bytes": {nb_bytes}, "k": {k}, "n": {n}}}"#);

        let bytes = random_bytes(nb_bytes);

        let setup_degree = k - 1;
        let nb_polynomials = nb_bytes / modulus_byte_size / k;
        let params = setup::<E, P>(setup_degree, nb_polynomials).expect("setup failed");
        let (_, verifier_key) = trim(params.kzg.clone(), setup_degree);
        let verifier_key = AplonkVerifierKey {
            vk_psi: verifier_key,
            tau_1: params.ipa.tau_1,
            g1: params.kzg.powers_of_g[0].into_group(),
            g2: params.kzg.h.into_group(),
        };

        let elements = field::split_data_into_field_elements::<E>(&bytes, k);
        let nb_polynomials = elements.len() / k;
        let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

        let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
        let points: Vec<E::ScalarField> = (0..n)
            .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        group.bench_function(&format!("commit|{bench_name}"), |b| {
            b.iter(|| {
                let (_, _) =
                    AplonkCommitment::<E, P>::commit(data.clone(), Some(params.clone())).unwrap();
            })
        });

        let commit = AplonkCommitment::<E, P>::commit(data.clone(), Some(params.clone())).unwrap();

        group.bench_function(&format!("prove|{bench_name}"), |b| {
            b.iter(|| {
                AplonkCommitment::<E, P>::prove(
                    commit.clone(),
                    data.clone(),
                    Some(&points),
                    Some(params.clone()),
                )
                .expect("prove failed")
            })
        });

        let blocks =
            AplonkCommitment::<E, P>::prove(commit, data, Some(&points), Some(params.clone()))
                .expect("prove failed");

        group.bench_function(&format!("verify_one|{bench_name}"), |b| {
            b.iter(|| {
                let block = blocks.get(0).unwrap();
                AplonkCommitment::<E, P>::verify(block, Some(verifier_key.clone())).unwrap();
            })
        });

        group.bench_function(&format!("verify_all|{bench_name}"), |b| {
            b.iter(|| {
                for block in &blocks {
                    AplonkCommitment::<E, P>::verify(block, Some(verifier_key.clone())).unwrap();
                }
            })
        });

        group.bench_function(&format!("batch_verify|{bench_name}"), |b| {
            b.iter(|| {
                AplonkCommitment::<E, P>::batch_verify(
                    blocks.as_slice(),
                    Some(verifier_key.clone()),
                )
                .unwrap();
            })
        });
    }

    group.finish();
}

fn aplonk_bls12(c: &mut Criterion) {
    aplonk_template::<Bls12_381, DensePolynomial<<Bls12_381 as Pairing>::ScalarField>>(
        c,
        "aPlonK Proof System (BLS-12-381)",
    );
}

fn aplonk_bn254(c: &mut Criterion) {
    aplonk_template::<Bn254, DensePolynomial<<Bn254 as Pairing>::ScalarField>>(
        c,
        "aPlonK Proof System (BN-254)",
    );
}

criterion_group!(benches, aplonk_bls12, aplonk_bn254);
criterion_main!(benches);
