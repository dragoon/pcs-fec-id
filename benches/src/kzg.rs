mod common;
use common::random_bytes;

use criterion::{criterion_group, criterion_main, Criterion};

use proofs::field;
use proofs::kzg::KzgCommitment;
use proofs::{trim, PolynomialCommitmentScheme};

use crate::common::BENCH_CASES;
use ark_bls12_381::Bls12_381;
use ark_bn254::Bn254;
use ark_ec::pairing::Pairing;
use ark_ff::PrimeField;
use ark_poly::univariate::DensePolynomial;
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::kzg10::KZG10;
use ark_std::ops::Div;
use ark_std::test_rng;
use rs_merkle::algorithms::Sha256;
use rs_merkle::Hasher;

fn kzg_template<E, P>(c: &mut Criterion, name: &str)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut group = c.benchmark_group(name);

    for (nb_bytes, k, n) in BENCH_CASES {
        let bench_name = format!(r#"{{"nb_bytes": {nb_bytes}, "k": {k}, "n": {n}}}"#);
        let bytes = random_bytes(nb_bytes);

        let setup_degree = nb_bytes / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        let rng = &mut test_rng();
        let params = KZG10::<E, P>::setup(setup_degree, false, rng).expect("setup failed");
        let (powers, verifier_key) = trim(params, setup_degree);

        let coefficients = field::split_data_into_field_elements::<E>(&bytes, k);
        let polynomial = P::from_coefficients_vec(coefficients);

        let data = (Sha256::hash(&bytes), bytes.len(), polynomial);
        let points: Vec<E::ScalarField> = (0..n)
            .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        group.bench_function(&format!("commit|{bench_name}"), |b| {
            b.iter(|| {
                KzgCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();
            })
        });

        let commit = KzgCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();

        group.bench_function(&format!("prove|{bench_name}"), |b| {
            b.iter(|| {
                KzgCommitment::<E, P>::prove(
                    commit.clone(),
                    data.clone(),
                    Some(&points),
                    Some(powers.clone()),
                )
                .expect("prove failed")
            })
        });

        let blocks =
            KzgCommitment::<E, P>::prove(commit, data, Some(&points), Some(powers)).unwrap();

        group.bench_function(&format!("batch_verify|{bench_name}"), |b| {
            b.iter(|| {
                KzgCommitment::<E, P>::batch_verify(blocks.as_slice(), Some(verifier_key.clone()))
                    .unwrap()
            })
        });

        group.bench_function(&format!("verify_one|{bench_name}"), |b| {
            b.iter(|| {
                let block = blocks.get(0).unwrap();
                KzgCommitment::<E, P>::verify(block, Some(verifier_key.clone())).unwrap();
            })
        });

        group.bench_function(&format!("verify_all|{bench_name}"), |b| {
            b.iter(|| {
                for block in &blocks {
                    KzgCommitment::<E, P>::verify(block, Some(verifier_key.clone())).unwrap();
                }
            })
        });
    }

    group.finish();
}

fn kzg_bls12(c: &mut Criterion) {
    kzg_template::<Bls12_381, DensePolynomial<<Bls12_381 as Pairing>::ScalarField>>(
        c,
        "KZG Proof System (BLS-12-381)",
    );
}

fn kzg_bn254(c: &mut Criterion) {
    kzg_template::<Bn254, DensePolynomial<<Bn254 as Pairing>::ScalarField>>(
        c,
        "KZG Proof System (BN-254)",
    );
}

criterion_group!(benches, kzg_bls12, kzg_bn254);
criterion_main!(benches);
