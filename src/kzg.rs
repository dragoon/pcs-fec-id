//! the classic KZG protocol
//!
//! > references
//! > - [Kate et al., 2010](https://link.springer.com/chapter/10.1007/978-3-642-17373-8_11)
use ark_ec::pairing::Pairing;
use ark_ec::AffineRepr;
use ark_ff::{BigInteger, PrimeField, Zero};
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::kzg10::{Commitment, Powers, Proof, Randomness, VerifierKey, KZG10};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize, Compress, SerializationError};
use ark_std::ops::Div;
use fec::Shard;
use rs_merkle::{algorithms::Sha256, Hasher};
use std::marker::PhantomData;

use crate::{aplonk_pc, u32_to_u8_vec, Error, PolynomialCommitmentScheme};

#[derive(Clone, Default, Debug, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct KZGBlock<E: Pairing> {
    shard: Shard,
    commit: Commitment<E>,
    proof: Proof<E>,
}

#[derive(Clone, Default, Debug, PartialEq)]
pub struct KzgCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    _engine: PhantomData<E>,
    _poly: PhantomData<P>,
}

impl<E, P> PolynomialCommitmentScheme for KzgCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    type Data = ([u8; 32], usize, P);
    type Point = E::ScalarField;
    type Commit = (Commitment<E>, Randomness<E::ScalarField, P>);
    type Proof = KZGBlock<E>;
    type Setup<'setup> = Powers<'setup, E>;
    type VerifierKey<'b> = VerifierKey<E>;

    fn commit(data: Self::Data, setup: Option<Self::Setup<'_>>) -> Result<Self::Commit, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (_, _, polynomial) = data;

        match KZG10::<E, P>::commit(&setup, &polynomial, None, None) {
            Ok(result) => Ok(result),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn prove(
        commit: Self::Commit,
        data: Self::Data,
        points: Option<&[Self::Point]>,
        setup: Option<Self::Setup<'_>>,
    ) -> Result<Vec<Self::Proof>, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (commit, randomness) = commit;
        let (hash, nb_bytes, polynomial) = data;

        let k = polynomial.coeffs().len();

        let shards = if let Some(pts) = points {
            pts.iter()
                .enumerate()
                .map(|(i, point)| Shard {
                    k: k as u32,
                    i: i as u32,
                    hash: hash.to_vec(),
                    bytes: polynomial.evaluate(point).into_bigint().to_bytes_le(),
                    size: nb_bytes,
                })
                .collect()
        } else {
            return Err(Error::ProofPointMissingError);
        };

        match prove::<E, P>(commit, randomness, polynomial, shards, setup) {
            Ok(blocks) => Ok(blocks),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn verify(
        proof: &Self::Proof,
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        match verify::<E, P>(proof, &verifier_key) {
            Ok(result) => Ok(result),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn batch_verify(
        proofs: &[Self::Proof],
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        match batch_verify(proofs, &verifier_key) {
            Ok(result) => Ok(result),
            Err(error) => Err(Error::SerializationError(error)),
        }
    }
}

fn prove<E, P>(
    commit: Commitment<E>,
    randomness: Randomness<E::ScalarField, P>,
    polynomial: P,
    shards: Vec<Shard>,
    powers: Powers<E>,
) -> Result<Vec<KZGBlock<E>>, ark_poly_commit::Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut proofs = Vec::new();
    for shard in shards {
        let point = E::ScalarField::from_le_bytes_mod_order(&[shard.i as u8]);

        match KZG10::<E, P>::open(&powers, &polynomial, point, &randomness) {
            Ok(proof) => proofs.push(KZGBlock {
                shard,
                commit,
                proof,
            }),
            Err(error) => return Err(error),
        }
    }

    Ok(proofs)
}

fn verify<E, P>(
    block: &KZGBlock<E>,
    verifier_key: &VerifierKey<E>,
) -> Result<bool, ark_poly_commit::Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let point = E::ScalarField::from_le_bytes_mod_order(&u32_to_u8_vec(block.shard.i));
    let value = E::ScalarField::from_le_bytes_mod_order(&block.shard.bytes);

    KZG10::<E, P>::check(verifier_key, &block.commit, point, value, &block.proof)
}

fn batch_verify<E, P>(
    blocks: &[KZGBlock<E>],
    verifier_key: &VerifierKey<E>,
) -> Result<bool, SerializationError>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut data = Vec::new();
    for block in blocks {
        block
            .proof
            .w
            .serialize_with_mode(&mut data, Compress::Yes)?
    }
    let hash = Sha256::hash(data.as_slice());
    let r = E::ScalarField::from_le_bytes_mod_order(&hash);
    let r_vec = aplonk_pc::algebra::powers_of::<E>(r, blocks.len());
    let commit = blocks[0].commit.0.into_group();

    let (proof_agg, inner_agg) = blocks.iter().enumerate().fold(
        (E::G1::zero(), E::G1::zero()),
        |(proof_acc, inner_acc), (i, block)| {
            let (y, c) = (
                E::ScalarField::from_le_bytes_mod_order(&block.shard.bytes),
                commit,
            );
            let alpha = E::ScalarField::from_le_bytes_mod_order(&block.shard.i.to_le_bytes());
            (
                proof_acc + block.proof.w * r_vec[i],
                inner_acc + (c - verifier_key.g * y + block.proof.w * alpha) * r_vec[i],
            )
        },
    );

    Ok(E::pairing(proof_agg, verifier_key.beta_h)
        == E::pairing(inner_agg, verifier_key.h.into_group()))
}

#[cfg(test)]
mod tests {
    use ark_bls12_381::Bls12_381;
    use ark_ec::pairing::Pairing;
    use ark_ff::{BigInteger, PrimeField};
    use ark_poly::univariate::DensePolynomial;
    use ark_poly::DenseUVPolynomial;
    use ark_poly_commit::kzg10::{VerifierKey, KZG10};
    use ark_std::test_rng;
    use fec::{decode, Shard};
    use reed_solomon_erasure::galois_prime::Field as GF;
    use rs_merkle::{algorithms::Sha256, Hasher};
    use std::ops::Div;

    use crate::kzg::{self, KZGBlock, KzgCommitment};
    use crate::{field, trim, PolynomialCommitmentScheme};

    type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;

    const FEC_K: usize = 8;
    const FEC_N: usize = 16;
    const REMOVE_SHARDS: usize = 4;

    fn bytes<E: Pairing>(k: usize) -> Vec<u8> {
        let nb_bytes = k * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        include_bytes!("../tests/dragoon_32x32.png")[0..nb_bytes].to_vec()
    }

    fn test_setup<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(Vec<KZGBlock<E>>, VerifierKey<E>), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<E, P>::setup(degree, false, rng)?;
        let (powers, verifier_key) = trim(params, degree);

        let coefficients = field::split_data_into_field_elements::<E>(bytes, k);
        let polynomial = P::from_coefficients_vec(coefficients);

        let (commit, randomness) = KZG10::<E, P>::commit(&powers, &polynomial, None, None).unwrap();

        let hash = Sha256::hash(bytes);
        let shards = (0..n)
            .map(|i| {
                let point = E::ScalarField::from_le_bytes_mod_order(&[i as u8]);
                Shard {
                    k: k as u32,
                    i: i as u32,
                    hash: hash.to_vec(),
                    bytes: polynomial.evaluate(&point).into_bigint().to_bytes_le(),
                    size: bytes.len(),
                }
            })
            .collect();

        let blocks = kzg::prove::<E, P>(commit, randomness, polynomial, shards, powers)?;

        Ok((blocks, verifier_key))
    }

    fn verify_template<E, P>(bytes: &[u8], k: usize, n: usize) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in &blocks {
            assert!(kzg::verify::<E, P>(block, &verifier_key).unwrap());
        }

        assert!(kzg::batch_verify(&blocks[1..3], &verifier_key).unwrap());

        Ok(())
    }

    /*
    fn corrupt_block<E: Pairing>(block: &KZGBlock<E>, byte: usize, value: u8) -> KZGBlock<E> {
        let mut bytes = bincode::serialize(&block.shard).unwrap();

        bytes[byte] = value;

        KZGBlock {
            shard: bincode::deserialize(&bytes).unwrap(),
            commit: block.commit,
            proof: block.proof,
        }
    }

    fn verify_with_errors_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        assert!(!kzg::verify::<E, P>(
            // from [`FECBLock`], the start of the data is after:
            //     u32 (4 bytes) +
            //     u32 (4 bytes) +
            //     vector length (8 bytes) +
            //     hash (32 bytes) +
            //     vector length (8 bytes)
            &corrupt_block::<E>(blocks.get(0).unwrap(), 4 + 4 + 8 + 32 + 8, 1),
            &verifier_key
        )
        .unwrap());

        Ok(())
    }
    */

    fn reconstruct_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
        l: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        assert!(
            n >= k + l,
            "kzg::tests::reconstruct_template requires at least {} = k (= {}) + l (= {}) shards, found {}",
            k + l,
            k,
            l,
            n
        );

        let (blocks, _) = test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        let mut shards = blocks.iter().map(|x| x.shard.clone()).collect::<Vec<_>>();

        for _ in 0..l {
            shards.remove(0);
        }

        assert_eq!(bytes.to_vec(), decode::<GF>(shards).unwrap());

        Ok(())
    }

    #[test]
    fn verify() {
        let bytes = bytes::<Bls12_381>(FEC_K);

        verify_template::<Bls12_381, UniPoly381>(&bytes, FEC_K, FEC_N)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], FEC_K, FEC_N)
            .expect("verification failed for bls12-381 with padding");
    }

    /*
    #[test]
    fn verify_with_errors() {
        let bytes = bytes::<Bls12_381>(FEC_K);

        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, FEC_K, FEC_N)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(
            &bytes[0..(bytes.len() - 33)],
            FEC_K,
            FEC_N,
        )
        .expect("verification failed for bls12-381 with padding");
    }
    */

    #[test]
    fn reconstruct() {
        let bytes = bytes::<Bls12_381>(FEC_K);

        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, FEC_K, FEC_N, REMOVE_SHARDS)
            .expect("reconstruction failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(
            &bytes[0..(bytes.len() - 33)],
            FEC_K,
            FEC_N,
            REMOVE_SHARDS,
        )
        .expect("reconstruction failed for bls12-381 with padding");
    }

    #[test]
    fn pc_interface() {
        let bytes = bytes::<Bls12_381>(FEC_K);
        let degree =
            bytes.len() / (<Bls12_381 as Pairing>::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<Bls12_381, UniPoly381>::setup(degree, false, rng).unwrap();
        let (powers, verifier_key) = trim(params, degree);

        let coefficients = field::split_data_into_field_elements::<Bls12_381>(&bytes, FEC_K);
        let polynomial = UniPoly381::from_coefficients_vec(coefficients);

        let data = (Sha256::hash(&bytes), bytes.len(), polynomial);
        let points: Vec<<Bls12_381 as Pairing>::ScalarField> = (0..FEC_N)
            .map(|i| <Bls12_381 as Pairing>::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        let commit =
            KzgCommitment::<Bls12_381, UniPoly381>::commit(data.clone(), Some(powers.clone()))
                .unwrap();

        // no setup given: Should return an error
        let _ =
            KzgCommitment::<Bls12_381, UniPoly381>::prove(commit.clone(), data.clone(), None, None)
                .unwrap_err();

        let blocks = KzgCommitment::<Bls12_381, UniPoly381>::prove(
            commit,
            data,
            Some(&points),
            Some(powers),
        )
        .unwrap();

        // no errors, should have N blocks
        assert_eq!(blocks.len(), FEC_N);

        for block in &blocks {
            assert!(KzgCommitment::<Bls12_381, UniPoly381>::verify(
                block,
                Some(verifier_key.clone())
            )
            .unwrap());
        }

        assert!(KzgCommitment::<Bls12_381, UniPoly381>::batch_verify(
            &blocks[1..3],
            Some(verifier_key)
        )
        .unwrap());
    }
}
