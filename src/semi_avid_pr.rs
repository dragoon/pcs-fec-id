//! Semi-AVID-PR: a simple protocol that does not require proofs
//!
//! > references:
//! > - [Nazirkhanova et al., 2021](https://dl.acm.org/doi/abs/10.1145/3558535.3559778)
use ark_ec::pairing::Pairing;
use ark_ff::{BigInteger, Field, PrimeField};
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::kzg10::{Commitment, Powers, KZG10};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize};
use fec::Shard;
use std::{
    marker::PhantomData,
    ops::{Div, Mul},
};

use crate::{kzg_multipoly::commit, Error, PolynomialCommitmentScheme};

#[derive(Debug, Clone, Default, PartialEq, CanonicalDeserialize, CanonicalSerialize)]
pub struct SemiAvidPRBlock<E: Pairing> {
    pub shard: Shard,
    commit: Vec<Commitment<E>>,
    m: usize,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct SemiAvidPrCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    _engine: PhantomData<E>,
    _poly: PhantomData<P>,
}

impl<E, P> PolynomialCommitmentScheme for SemiAvidPrCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    type Data = ([u8; 32], usize, Vec<P>);
    type Point = E::ScalarField;
    type Commit = Vec<Commitment<E>>;
    type Proof = SemiAvidPRBlock<E>;
    type Setup<'setup> = Powers<'setup, E>;
    type VerifierKey<'a> = Powers<'a, E>;

    fn commit(data: Self::Data, setup: Option<Self::Setup<'_>>) -> Result<Self::Commit, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (_, _, polynomials) = data;

        let polynomials_to_commit = (0..polynomials[0].coeffs().len())
            .map(|i| P::from_coefficients_vec(polynomials.iter().map(|p| p.coeffs()[i]).collect()))
            .collect::<Vec<P>>();

        match commit(&setup, &polynomials_to_commit) {
            Ok((commits, _)) => Ok(commits),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn prove(
        commit: Self::Commit,
        data: Self::Data,
        points: Option<&[Self::Point]>,
        _setup: Option<Self::Setup<'_>>,
    ) -> Result<Vec<Self::Proof>, Error> {
        let (hash, nb_bytes, polynomials) = data;

        if points.is_none() {
            return Err(Error::ProofPointMissingError);
        }

        match prove::<E, P>(commit, hash, nb_bytes, polynomials, points.unwrap()) {
            Ok(blocks) => Ok(blocks),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn verify(
        proof: &Self::Proof,
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        Ok(verify::<E, P>(proof, &verifier_key))
    }

    fn batch_verify(
        proofs: &[Self::Proof],
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        Ok(batch_verify(proofs, &verifier_key))
    }
}

fn prove<E, P>(
    commits: Vec<Commitment<E>>,
    hash: [u8; 32],
    nb_bytes: usize,
    polynomials: Vec<P>,
    points: &[E::ScalarField],
) -> Result<Vec<SemiAvidPRBlock<E>>, ark_poly_commit::Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let k = polynomials[0].coeffs().len();

    let evaluations = points
        .iter()
        .map(|point| polynomials.iter().map(|p| p.evaluate(point)).collect())
        .collect::<Vec<Vec<E::ScalarField>>>();

    let mut proofs = Vec::new();
    for (i, row) in evaluations.iter().enumerate() {
        let mut shard = vec![];
        for r in row {
            shard.append(&mut r.into_bigint().to_bytes_le());
        }

        proofs.push(SemiAvidPRBlock {
            shard: Shard {
                k: k as u32,
                i: i as u32,
                hash: hash.to_vec(),
                bytes: shard,
                size: nb_bytes,
            },
            commit: commits.clone(),
            m: polynomials.len(),
        })
    }

    Ok(proofs)
}

fn verify<E, P>(block: &SemiAvidPRBlock<E>, verifier_key: &Powers<E>) -> bool
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let alpha = E::ScalarField::from_le_bytes_mod_order(&[block.shard.i as u8]);

    let mut elements = Vec::new();
    for chunk in block
        .shard
        .bytes
        .chunks((E::ScalarField::MODULUS_BIT_SIZE as usize) / 8 + 1)
    {
        elements.push(E::ScalarField::from_le_bytes_mod_order(chunk));
    }
    let polynomial = P::from_coefficients_vec(elements);
    let (commit, _) = KZG10::<E, P>::commit(verifier_key, &polynomial, None, None).unwrap();

    Into::<E::G1>::into(commit.0)
        == block
            .commit
            .iter()
            .enumerate()
            .map(|(j, c)| {
                let commit: E::G1 = c.0.into();
                commit.mul(alpha.pow([j as u64]))
            })
            .sum()
}

fn batch_verify<E, P>(blocks: &[SemiAvidPRBlock<E>], verifier_key: &Powers<E>) -> bool
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    for block in blocks {
        if !verify(block, verifier_key) {
            return false;
        }
    }

    true
}

#[cfg(test)]
mod tests {
    use crate::kzg_multipoly::commit;
    use crate::semi_avid_pr::{SemiAvidPRBlock, SemiAvidPrCommitment};
    use crate::{field, semi_avid_pr, trim, PolynomialCommitmentScheme};

    use ark_bls12_381::Bls12_381;
    use ark_ec::pairing::Pairing;
    use ark_ff::{Field, PrimeField};
    use ark_poly::{univariate::DensePolynomial, DenseUVPolynomial};
    use ark_poly_commit::kzg10::{Commitment, Powers, KZG10};
    use ark_std::test_rng;
    use fec::decode;
    use reed_solomon_erasure::galois_prime::Field as GF;
    use rs_merkle::{algorithms::Sha256, Hasher};
    use std::ops::{Div, Mul};

    type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;

    fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
        let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
    }

    fn test_setup<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(Vec<SemiAvidPRBlock<E>>, Powers<E>), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<E, P>::setup(degree, false, rng)?;
        let (powers, _) = trim(params, degree);

        let elements = field::split_data_into_field_elements::<E>(bytes, k);
        let nb_polynomials = elements.len() / k;
        let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

        let polynomials_to_commit = (0..polynomials[0].coeffs().len())
            .map(|i| P::from_coefficients_vec(polynomials.iter().map(|p| p.coeffs()[i]).collect()))
            .collect::<Vec<P>>();

        let (commits, _) = commit(&powers, &polynomials_to_commit).unwrap();

        let points: Vec<E::ScalarField> = (0..n)
            .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        let hash = Sha256::hash(bytes);

        let blocks = semi_avid_pr::prove::<E, P>(commits, hash, bytes.len(), polynomials, &points)
            .expect("Semi-AVID-PR proof failed");

        Ok((blocks, powers))
    }

    fn verify_template<E, P>(bytes: &[u8], k: usize, n: usize) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in &blocks {
            assert!(semi_avid_pr::verify::<E, P>(block, &verifier_key));
        }

        assert!(semi_avid_pr::batch_verify(&blocks[1..3], &verifier_key));

        Ok(())
    }

    #[test]
    fn verify_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[ignore = "Semi-AVID-PR does not support large padding"]
    #[test]
    fn verify_with_large_padding_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[ignore = "Semi-AVID-PR does not support large padding"]
    #[test]
    fn verify_with_large_padding_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[ignore = "Semi-AVID-PR does not support large padding"]
    #[test]
    fn verify_with_large_padding_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    fn verify_with_errors_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in &blocks {
            assert!(semi_avid_pr::verify::<E, P>(block, &verifier_key));
        }

        let mut corrupted_block = blocks[0].clone();
        // modify a field in the struct b to corrupt the block proof without corrupting the data serialization
        let a = E::ScalarField::from_le_bytes_mod_order(&[123]);
        let mut commits: Vec<E::G1> = corrupted_block.commit.iter().map(|c| c.0.into()).collect();
        commits[0] = commits[0].mul(a.pow([4321_u64]));
        corrupted_block.commit = commits.iter().map(|&c| Commitment(c.into())).collect();

        assert!(!semi_avid_pr::verify::<E, P>(
            &corrupted_block,
            &verifier_key
        ));

        // let's build some blocks containing errors
        let mut blocks_with_errors = Vec::new();

        let b3 = blocks.get(3).unwrap();
        blocks_with_errors.push(SemiAvidPRBlock {
            shard: b3.shard.clone(),
            commit: b3.commit.clone(),
            m: b3.m,
        });
        assert!(semi_avid_pr::batch_verify(
            blocks_with_errors.as_slice(),
            &verifier_key
        ));

        blocks_with_errors.push(corrupted_block);
        assert!(!semi_avid_pr::batch_verify(
            blocks_with_errors.as_slice(),
            &verifier_key
        ));

        Ok(())
    }

    #[test]
    fn verify_with_errors_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_with_errors_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_with_errors_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    fn reconstruct_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
        l: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        assert!(
            n >= k + l,
            "semi_avid_pr::tests::reconstruct_template requires at least {} = k (= {}) + l (= {}) shards, found {}",
            k + l,
            k,
            l,
            n
        );

        let (blocks, _) = test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        let mut shards = blocks.iter().map(|x| x.shard.clone()).collect::<Vec<_>>();

        for _ in 0..l {
            shards.remove(0);
        }

        assert_eq!(bytes.to_vec(), decode::<GF>(shards).unwrap());

        Ok(())
    }

    #[test]
    fn reconstruct_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 10)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn pc_interface() {
        let bytes = bytes::<Bls12_381>(8, 2);
        let degree =
            bytes.len() / (<Bls12_381 as Pairing>::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<Bls12_381, UniPoly381>::setup(degree, false, rng).unwrap();
        let (powers, _) = trim(params, degree);

        let elements = field::split_data_into_field_elements::<Bls12_381>(&bytes, 8);
        let nb_polynomials = elements.len() / 8;
        let polynomials = field::build_interleaved_polynomials::<Bls12_381, UniPoly381>(
            &elements,
            nb_polynomials,
        );

        let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
        let points: Vec<<Bls12_381 as Pairing>::ScalarField> = (0..16)
            .map(|i| <Bls12_381 as Pairing>::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        let commit = SemiAvidPrCommitment::<Bls12_381, UniPoly381>::commit(
            data.clone(),
            Some(powers.clone()),
        )
        .unwrap();

        // no setup given: Should return an error
        let _ = SemiAvidPrCommitment::<Bls12_381, UniPoly381>::prove(
            commit.clone(),
            data.clone(),
            None,
            None,
        )
        .unwrap_err();

        let blocks = SemiAvidPrCommitment::<Bls12_381, UniPoly381>::prove(
            commit,
            data,
            Some(&points),
            Some(powers.clone()),
        )
        .unwrap();

        // no errors, should have N blocks
        assert_eq!(blocks.len(), 16);

        for block in &blocks {
            assert!(SemiAvidPrCommitment::<Bls12_381, UniPoly381>::verify(
                block,
                Some(powers.clone())
            )
            .unwrap());
        }

        assert!(SemiAvidPrCommitment::<Bls12_381, UniPoly381>::batch_verify(
            &blocks[1..3],
            Some(powers),
        )
        .unwrap());
    }
}
