use std::fmt::{Display, Formatter};

use ark_ec::pairing::Pairing;
use ark_poly_commit::kzg10::{Powers, UniversalParams, VerifierKey};

pub mod aplonk_pc;
pub mod field;
pub mod kzg;
pub mod kzg_multipoly;
pub mod semi_avid_pr;

// NOTE: [`ark_poly_commit::Error`] and [`ark_serialize::SerializationError`] don't implement
// `Clone`, `Default` and `PartialEq`...
#[derive(Debug)]
pub enum Error {
    SetupError,
    VerifierKeyError,
    ArkError(ark_poly_commit::Error),
    SerializationError(ark_serialize::SerializationError),
    MerkleError(String),
    NotEnoughAplonkPolynomials,
    EncodingError,
    PolynomialCountIpaError(String),
    EllipticInverseError,
    ProofPointMissingError,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::SetupError => write!(f, "Bad setup given"),
            Error::VerifierKeyError => write!(f, "Bad verifier key"),
            Error::MerkleError(message) => write!(f, "{}", message),
            Error::ArkError(err) => write!(f, "{}", err),
            Error::SerializationError(err) => write!(f, "{}", err),
            Error::NotEnoughAplonkPolynomials => {
                write!(f, "Not enough polynomials given to aPlonK")
            }
            Error::EncodingError => write!(f, "Could not encode the data"),
            Error::PolynomialCountIpaError(message) => write!(f, "{}", message),
            Error::EllipticInverseError => write!(f, "one of the aPlonK hash was 0"),
            Error::ProofPointMissingError => write!(f, "no proof point given"),
        }
    }
}

impl std::error::Error for Error {}

/// the common interface to use any of the four proving scheme
///
/// # example
/// below is an example of how to use the common interface with Semi-AVID-PR.
///
/// - first let's import all the objects we will need
/// ```rust
/// use std::ops::Div;
///
/// use ark_bls12_381::Bls12_381;
/// use ark_ec::pairing::Pairing;
/// use ark_ff::PrimeField;
/// use ark_poly_commit::{kzg10::KZG10, DenseUVPolynomial};
/// use ark_poly::univariate::DensePolynomial;
/// use ark_std::test_rng;
/// use rs_merkle::{algorithms::Sha256, Hasher};
///
/// use proofs::{field, semi_avid_pr::SemiAvidPrCommitment, trim, PolynomialCommitmentScheme};
/// ```
/// > **Note**  
/// > the following will appear to run as-is, without a `main`, but internally, there is a generic
/// > `run` function that is called in a `main`.
/// >
/// > for the sake of simplicity, these details have been ommitted here.
/// - then we can create the trusted setup
/// ```rust
/// # use std::ops::Div;
/// #
/// # use ark_bls12_381::Bls12_381;
/// # use ark_ec::pairing::Pairing;
/// # use ark_ff::PrimeField;
/// # use ark_poly_commit::{kzg10::KZG10, DenseUVPolynomial};
/// # use ark_poly::univariate::DensePolynomial;
/// # use ark_std::test_rng;
/// # use rs_merkle::{algorithms::Sha256, Hasher};
/// #
/// # use proofs::{field, semi_avid_pr::SemiAvidPrCommitment, trim, PolynomialCommitmentScheme};
/// #
/// # type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;
/// #
/// # fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
/// #     let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #     include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
/// # }
/// #
/// # fn run<E, P>(k: usize, n: usize)
/// # where
/// #     E: Pairing,
/// #     P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
/// #     for<'a, 'b> &'a P: Div<&'b P, Output = P>,
/// # {
/// # let bytes = bytes::<E>(8, 2);
/// let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
///
/// let rng = &mut test_rng();
///
/// let params = KZG10::<E, P>::setup(degree, false, rng).unwrap();
/// let (powers, _) = trim(params, degree);
/// # }
/// #
/// # fn main() {
/// #     run::<Bls12_381, UniPoly381>(8, 16);
/// # }
/// ```
/// - then we create the polynomials and the evaluation points of the FEC encoding
/// ```rust
/// # use std::ops::Div;
/// #
/// # use ark_bls12_381::Bls12_381;
/// # use ark_ec::pairing::Pairing;
/// # use ark_ff::PrimeField;
/// # use ark_poly_commit::{kzg10::KZG10, DenseUVPolynomial};
/// # use ark_poly::univariate::DensePolynomial;
/// # use ark_std::test_rng;
/// # use rs_merkle::{algorithms::Sha256, Hasher};
/// #
/// # use proofs::{field, semi_avid_pr::SemiAvidPrCommitment, trim, PolynomialCommitmentScheme};
/// #
/// # type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;
/// #
/// # fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
/// #     let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #     include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
/// # }
/// #
/// # fn run<E, P>(k: usize, n: usize)
/// # where
/// #     E: Pairing,
/// #     P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
/// #     for<'a, 'b> &'a P: Div<&'b P, Output = P>,
/// # {
/// # let bytes = bytes::<E>(8, 2);
/// # let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #
/// # let rng = &mut test_rng();
/// #
/// # let params = KZG10::<E, P>::setup(degree, false, rng).unwrap();
/// # let (powers, _) = trim(params, degree);
/// #
/// let elements = field::split_data_into_field_elements::<E>(&bytes, k);
/// let nb_polynomials = elements.len() / k;
/// let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);
///
/// let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
/// let points: Vec<E::ScalarField> = (0..n)
///     .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
///     .collect();
/// # }
/// #
/// # fn main() {
/// #     run::<Bls12_381, UniPoly381>(8, 16);
/// # }
/// ```
/// - we can now commit and prove the blocks
/// ```rust
/// # use std::ops::Div;
/// #
/// # use ark_bls12_381::Bls12_381;
/// # use ark_ec::pairing::Pairing;
/// # use ark_ff::PrimeField;
/// # use ark_poly_commit::{kzg10::KZG10, DenseUVPolynomial};
/// # use ark_poly::univariate::DensePolynomial;
/// # use ark_std::test_rng;
/// # use rs_merkle::{algorithms::Sha256, Hasher};
/// #
/// # use proofs::{field, semi_avid_pr::SemiAvidPrCommitment, trim, PolynomialCommitmentScheme};
/// #
/// # type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;
/// #
/// # fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
/// #     let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #     include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
/// # }
/// #
/// # fn run<E, P>(k: usize, n: usize)
/// # where
/// #     E: Pairing,
/// #     P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
/// #     for<'a, 'b> &'a P: Div<&'b P, Output = P>,
/// # {
/// # let bytes = bytes::<E>(8, 2);
/// # let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #
/// # let rng = &mut test_rng();
/// #
/// # let params = KZG10::<E, P>::setup(degree, false, rng).unwrap();
/// # let (powers, _) = trim(params, degree);
/// #
/// # let elements = field::split_data_into_field_elements::<E>(&bytes, k);
/// # let nb_polynomials = elements.len() / k;
/// # let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);
/// #
/// # let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
/// # let points: Vec<E::ScalarField> = (0..n)
/// #     .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
/// #     .collect();
/// #
/// let commit = SemiAvidPrCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();
///
/// let blocks =
///     SemiAvidPrCommitment::<E, P>::prove(commit, data, Some(&points), Some(powers.clone()))
///         .unwrap();
/// # }
/// #
/// # fn main() {
/// #     run::<Bls12_381, UniPoly381>(8, 16);
/// # }
/// ```
/// - and finally, make sure they can be properly verified
/// ```rust
/// # use std::ops::Div;
/// #
/// # use ark_bls12_381::Bls12_381;
/// # use ark_ec::pairing::Pairing;
/// # use ark_ff::PrimeField;
/// # use ark_poly_commit::{kzg10::KZG10, DenseUVPolynomial};
/// # use ark_poly::univariate::DensePolynomial;
/// # use ark_std::test_rng;
/// # use rs_merkle::{algorithms::Sha256, Hasher};
/// #
/// # use proofs::{field, semi_avid_pr::SemiAvidPrCommitment, trim, PolynomialCommitmentScheme};
/// #
/// # type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;
/// #
/// # fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
/// #     let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #     include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
/// # }
/// #
/// # fn run<E, P>(k: usize, n: usize)
/// # where
/// #     E: Pairing,
/// #     P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
/// #     for<'a, 'b> &'a P: Div<&'b P, Output = P>,
/// # {
/// # let bytes = bytes::<E>(8, 2);
/// # let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
/// #
/// # let rng = &mut test_rng();
/// #
/// # let params = KZG10::<E, P>::setup(degree, false, rng).unwrap();
/// # let (powers, _) = trim(params, degree);
/// #
/// # let elements = field::split_data_into_field_elements::<E>(&bytes, k);
/// # let nb_polynomials = elements.len() / k;
/// # let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);
/// #
/// # let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
/// # let points: Vec<E::ScalarField> = (0..n)
/// #     .map(|i| E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
/// #     .collect();
/// #
/// # let commit = SemiAvidPrCommitment::<E, P>::commit(data.clone(), Some(powers.clone())).unwrap();
/// #
/// # let blocks =
/// #     SemiAvidPrCommitment::<E, P>::prove(commit, data, Some(&points), Some(powers.clone()))
/// #         .unwrap();
/// assert_eq!(blocks.len(), n);
///
/// for block in &blocks {
///     assert!(SemiAvidPrCommitment::<E, P>::verify(block, Some(powers.clone())).unwrap());
/// }
///
/// assert!(SemiAvidPrCommitment::<E, P>::batch_verify(&blocks[1..3], Some(powers),).unwrap());
/// # }
/// #
/// # fn main() {
/// #     run::<Bls12_381, UniPoly381>(8, 16);
/// # }
/// ```
pub trait PolynomialCommitmentScheme {
    type Data;
    type Point;
    type Commit;
    type Proof;
    type Setup<'a>;
    type VerifierKey<'b>;

    fn commit(data: Self::Data, setup: Option<Self::Setup<'_>>) -> Result<Self::Commit, Error>;

    fn prove(
        commit: Self::Commit,
        data: Self::Data,
        points: Option<&[Self::Point]>,
        setup: Option<Self::Setup<'_>>,
    ) -> Result<Vec<Self::Proof>, Error>;

    fn verify(
        proof: &Self::Proof,
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error>;

    fn batch_verify(
        proofs: &[Self::Proof],
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error>;
}

/// Specializes the public parameters for a given maximum degree `d` for polynomials
/// `d` should be less that `pp.max_degree()`.
///
/// > see [`ark-poly-commit::kzg10::tests::KZG10`](https://github.com/jdetchart/poly-commit/blob/master/src/kzg10/mod.rs#L509)
pub fn trim<E: Pairing>(
    pp: UniversalParams<E>,
    supported_degree: usize,
) -> (Powers<'static, E>, VerifierKey<E>) {
    let powers_of_g = pp.powers_of_g[..=supported_degree].to_vec();
    let powers_of_gamma_g = (0..=supported_degree)
        .map(|i| pp.powers_of_gamma_g[&i])
        .collect();

    let powers = Powers {
        powers_of_g: ark_std::borrow::Cow::Owned(powers_of_g),
        powers_of_gamma_g: ark_std::borrow::Cow::Owned(powers_of_gamma_g),
    };
    let vk = VerifierKey {
        g: pp.powers_of_g[0],
        gamma_g: pp.powers_of_gamma_g[&0],
        h: pp.h,
        beta_h: pp.beta_h,
        prepared_h: pp.prepared_h.clone(),
        prepared_beta_h: pp.prepared_beta_h.clone(),
    };

    (powers, vk)
}

fn u32_to_u8_vec(num: u32) -> Vec<u8> {
    vec![
        (num & 0xFF) as u8,
        ((num >> 8) & 0xFF) as u8,
        ((num >> 16) & 0xFF) as u8,
        ((num >> 24) & 0xFF) as u8,
    ]
}

#[cfg(test)]
mod tests {
    use super::u32_to_u8_vec;

    #[test]
    fn u32_to_u8_convertion() {
        assert_eq!(u32_to_u8_vec(0u32), vec![0u8, 0u8, 0u8, 0u8]);
        assert_eq!(u32_to_u8_vec(1u32), vec![1u8, 0u8, 0u8, 0u8]);
        assert_eq!(u32_to_u8_vec(256u32), vec![0u8, 1u8, 0u8, 0u8]);
        assert_eq!(u32_to_u8_vec(65536u32), vec![0u8, 0u8, 1u8, 0u8]);
        assert_eq!(u32_to_u8_vec(16777216u32), vec![0u8, 0u8, 0u8, 1u8]);
    }
}
