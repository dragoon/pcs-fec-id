//! KZG+: the multipolynomial and non-interactive extension of KZG
//!
//! > references:
//! > - [Kate et al., 2010](https://link.springer.com/chapter/10.1007/978-3-642-17373-8_11)
//! > - [Boneh et al., 2020](https://eprint.iacr.org/2020/081)
use ark_ec::{pairing::Pairing, AffineRepr};
use ark_ff::{BigInteger, PrimeField};
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::{
    kzg10::{Commitment, Powers, Proof, Randomness, VerifierKey, KZG10},
    PCRandomness,
};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize, Compress, SerializationError};
use ark_std::{ops::Div, Zero};
use fec::Shard;
use rs_merkle::{algorithms::Sha256, Hasher};
use std::marker::PhantomData;
use std::ops::{AddAssign, Mul};

use crate::{aplonk_pc::algebra, u32_to_u8_vec, Error, PolynomialCommitmentScheme};

#[allow(clippy::type_complexity)]
pub(super) fn commit<E, P>(
    powers: &Powers<E>,
    polynomials: &[P],
) -> Result<(Vec<Commitment<E>>, Vec<Randomness<E::ScalarField, P>>), ark_poly_commit::Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut commits = Vec::new();
    let mut randomnesses = Vec::new();
    for polynomial in polynomials {
        let (commit, randomness) = KZG10::<E, P>::commit(powers, polynomial, None, None)?;
        commits.push(commit);
        randomnesses.push(randomness);
    }

    Ok((commits, randomnesses))
}

#[derive(Debug, Clone, Default, PartialEq, CanonicalDeserialize, CanonicalSerialize)]
pub struct KZGMultipolyBlock<E: Pairing> {
    pub shard: Shard,
    commit: Vec<Commitment<E>>,
    proof: Proof<E>,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct KzgMultiCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    _engine: PhantomData<E>,
    _poly: PhantomData<P>,
}

impl<E, P> PolynomialCommitmentScheme for KzgMultiCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    type Data = ([u8; 32], usize, Vec<P>);
    type Point = E::ScalarField;
    type Commit = Vec<Commitment<E>>;
    type Proof = KZGMultipolyBlock<E>;
    type Setup<'setup> = Powers<'setup, E>;
    type VerifierKey<'b> = VerifierKey<E>;

    fn commit(data: Self::Data, setup: Option<Self::Setup<'_>>) -> Result<Self::Commit, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (_, _, polynomials) = data;

        match commit(&setup, &polynomials) {
            Ok((commits, _)) => Ok(commits),
            Err(error) => Err(Error::ArkError(error)),
        }
    }

    fn prove(
        commit: Self::Commit,
        data: Self::Data,
        points: Option<&[Self::Point]>,
        setup: Option<Self::Setup<'_>>,
    ) -> Result<Vec<Self::Proof>, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (hash, nb_bytes, polynomials) = data;

        let k = polynomials[0].coeffs().len();

        let shards = if let Some(pts) = points {
            pts.iter()
                .enumerate()
                .map(|(i, point)| {
                    let mut shard = vec![];
                    for p in &polynomials {
                        shard.append(&mut p.evaluate(point).into_bigint().to_bytes_le());
                    }
                    Shard {
                        k: k as u32,
                        i: i as u32,
                        hash: hash.to_vec(),
                        bytes: shard,
                        size: nb_bytes,
                    }
                })
                .collect()
        } else {
            return Err(Error::ProofPointMissingError);
        };

        prove::<E, P>(commit, polynomials, shards, setup)
    }

    fn verify(
        proof: &Self::Proof,
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        Ok(verify(proof, &verifier_key))
    }

    fn batch_verify(
        proofs: &[Self::Proof],
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        match batch_verify(proofs, &verifier_key) {
            Ok(result) => Ok(result),
            Err(error) => Err(Error::SerializationError(error)),
        }
    }
}

/// this function splits the data (bytes) into k shards and generates n shards with a proof for each.
/// First, generate m polynomials with k coefficients (meaning degree is k-1)
/// Then, for each shard (n):
///     evaluate the m polynomials in one point (alpha)
///     compute a hash of the concatenated evaluations
///     compute Q(X) = sum_{i=0}^{m-1}{r^i * P_i(X)}
///     prove this polynomial with KZG10
///     store in the n KZGMultipolyBlock the proof, the m commits and the m P_i evaluations
fn prove<E, P>(
    commits: Vec<Commitment<E>>,
    polynomials: Vec<P>,
    shards: Vec<Shard>,
    powers: Powers<E>,
) -> Result<Vec<KZGMultipolyBlock<E>>, Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    // step 3. each shard will contain an evaluation of each polynomial
    // in i (the alpha corresponding to the matrix column)
    // and the commit of each polynomials
    // compute a random combination of the polynomials and compute a proof for this polynomial
    let mut proofs = Vec::new();
    for shard in shards {
        let point = E::ScalarField::from_le_bytes_mod_order(&[shard.i as u8]);

        let mut eval_bytes = vec![];
        for p in &polynomials {
            let elt = p.evaluate(&point);
            if let Err(error) = elt.serialize_with_mode(&mut eval_bytes, Compress::Yes) {
                return Err(Error::SerializationError(error));
            }
        }

        let hash = Sha256::hash(eval_bytes.as_slice());
        let r = E::ScalarField::from_le_bytes_mod_order(&hash);

        let r_vec = algebra::powers_of::<E>(r, polynomials.len());
        let poly_q = algebra::scalar_product_polynomial::<E, P>(&r_vec, &polynomials);

        match KZG10::<E, P>::open(
            &powers,
            &poly_q,
            point,
            &Randomness::<E::ScalarField, P>::empty(),
        ) {
            Ok(proof) => proofs.push(KZGMultipolyBlock {
                shard,
                commit: commits.clone(),
                proof,
            }),
            Err(error) => return Err(Error::ArkError(error)),
        };
    }

    Ok(proofs)
}

fn compute_data_for_one_shard<E, P>(block: &KZGMultipolyBlock<E>) -> (E::ScalarField, E::G1)
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let data = &block.shard.bytes;

    let mut elements = Vec::new();
    let chunks = data.chunks(((E::ScalarField::MODULUS_BIT_SIZE + 7) as usize) / 8);
    for chunk in chunks {
        elements.push(E::ScalarField::from_le_bytes_mod_order(chunk));
    }

    let hash = Sha256::hash(data);
    let r = E::ScalarField::from_le_bytes_mod_order(&hash);
    let r_vec = algebra::powers_of::<E>(r, elements.len());

    // compute y and c
    let mut y = E::ScalarField::zero();
    let mut c = E::G1::zero();
    for (i, (shard, commit)) in elements.iter().zip(block.commit.iter()).enumerate() {
        y.add_assign(shard.mul(r_vec[i]));
        c.add_assign(commit.0.mul(r_vec[i]));
    }

    (y, c)
}

/// for a given KZGMultipolyBlock, verify that the data has been correctly generated
/// First, transform data bytes into m polynomial evaluation
/// compute the hash of the concatenation of these evaluations
/// compute y as a combination of the shards: y = sum(r^i * Shard_i) for i=[0..m[
/// compute c as a combination of the commitments: c = sum(r^i * Commit_i) for i=[0..m[
/// Check if e(c - yG1,G2) == e(proof,(T-alpha)G2)
fn verify<E, P>(block: &KZGMultipolyBlock<E>, verifier_key: &VerifierKey<E>) -> bool
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let (y, c) = compute_data_for_one_shard(block);

    let p1 = c - verifier_key.g.mul(y);
    let inner = verifier_key.beta_h.into_group()
        - verifier_key
            .h
            .mul(&E::ScalarField::from_le_bytes_mod_order(&u32_to_u8_vec(
                block.shard.i,
            )));

    E::pairing(p1, verifier_key.h) == E::pairing(block.proof.w, inner)
}

/// verify a bunch of blocks at once using a single elliptic pairing.
///
/// Rather than checking
///     e(c - yG_1, G_2) = e(proof, (\tau - \alpha)G_2)
/// for each block individually (see [`verify`]),
/// we combine the blocks and perform one pairing as follows:
///
/// > **Note**
/// > let's define
/// > - $m$ as the number of polynomials in the data
/// > - $k$ as the number of blocks given
///
/// 1. compute $r$ as the hash of all the proofs
/// 2. for each block b_i:
///    - compute y_i = sum_{j=[0..m[}(r^j * Shard_j)
///    - compute c_i = sum_{j=[0..m[}(r^j * Commit_j)
/// 3. combine a combination of proofs and (y, c, \alpha) such as :
///    proof_agg = sum_{i=[0..k[}(r^i * proof_i)
///    inner_agg = sum_{i=[0..k[}(r^i * (c_i - y_i G_1 + alpha_i * proof_i))
/// 4. check e(proof_agg, \tau G_2) = e(inner_agg, G_2)
fn batch_verify<E, P>(
    blocks: &[KZGMultipolyBlock<E>],
    verifier_key: &VerifierKey<E>,
) -> Result<bool, SerializationError>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let mut data = Vec::new();
    for b in blocks {
        b.proof.w.serialize_with_mode(&mut data, Compress::Yes)?
    }
    let hash = Sha256::hash(data.as_slice());
    let r = E::ScalarField::from_le_bytes_mod_order(&hash);
    let r_vec = algebra::powers_of::<E>(r, blocks.len());

    let (proof_agg, inner_agg) = blocks.iter().enumerate().fold(
        (E::G1::zero(), E::G1::zero()),
        |(proof_acc, inner_acc), (i, block)| {
            let (y, c) = compute_data_for_one_shard(block);
            let alpha = E::ScalarField::from_le_bytes_mod_order(&block.shard.i.to_le_bytes());
            (
                proof_acc + block.proof.w * r_vec[i],
                inner_acc + (c - verifier_key.g * y + block.proof.w * alpha) * r_vec[i],
            )
        },
    );

    // e(sum(r^i * proof_i, T * g2) = e(sum(r^i * (commit_i  - y_i * g1 + alpha_i * proof_i)),g2)
    Ok(E::pairing(proof_agg, verifier_key.beta_h)
        == E::pairing(inner_agg, verifier_key.h.into_group()))
}

#[cfg(test)]
mod tests {
    use crate::kzg_multipoly::{self, KZGMultipolyBlock, KzgMultiCommitment};
    use crate::{field, trim, PolynomialCommitmentScheme};
    use ark_bls12_381::Bls12_381;
    use ark_ec::pairing::Pairing;
    use ark_ff::{BigInteger, PrimeField};
    use ark_poly::{univariate::DensePolynomial, DenseUVPolynomial};
    use ark_poly_commit::kzg10::{VerifierKey, KZG10};
    use ark_std::test_rng;
    use fec::{decode, Shard};
    use reed_solomon_erasure::galois_prime::Field as GF;
    use rs_merkle::{algorithms::Sha256, Hasher};
    use std::ops::Div;

    type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;

    fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
        let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        include_bytes!("../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
    }

    fn test_setup<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(Vec<KZGMultipolyBlock<E>>, VerifierKey<E>), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let degree = bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<E, P>::setup(degree, false, rng)?;
        let (powers, verifier_key) = trim(params, degree);

        let elements = field::split_data_into_field_elements::<E>(bytes, k);
        let nb_polynomials = elements.len() / k;
        let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

        let (commits, _) = kzg_multipoly::commit(&powers, &polynomials).unwrap();

        let hash = Sha256::hash(bytes);
        let shards = (0..n)
            .map(|i| {
                let mut shard = vec![];
                for p in &polynomials {
                    shard.append(
                        &mut p
                            .evaluate(&E::ScalarField::from_le_bytes_mod_order(&[i as u8]))
                            .into_bigint()
                            .to_bytes_le(),
                    );
                }
                Shard {
                    k: k as u32,
                    i: i as u32,
                    hash: hash.to_vec(),
                    bytes: shard,
                    size: bytes.len(),
                }
            })
            .collect();

        let blocks = kzg_multipoly::prove::<E, P>(commits, polynomials, shards, powers)
            .expect("KZG+ proof failed");

        Ok((blocks, verifier_key))
    }

    fn verify_template<E, P>(bytes: &[u8], k: usize, n: usize) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in &blocks {
            assert!(kzg_multipoly::verify::<E, P>(block, &verifier_key));
        }

        assert!(kzg_multipoly::batch_verify(&blocks[1..3], &verifier_key).unwrap());

        Ok(())
    }

    /*
    fn corrupt_block<E: Pairing>(
        block: &KZGMultipolyBlock<E>,
        byte: usize,
        value: u8,
    ) -> KZGMultipolyBlock<E> {
        let mut bytes = bincode::serialize(&block.shard).unwrap();

        bytes[byte] = value;

        KZGMultipolyBlock {
            shard: bincode::deserialize(&bytes).unwrap(),
            commit: block.commit.clone(),
            proof: block.proof,
        }
    }

    fn verify_with_errors_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, verifier_key) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in &blocks {
            assert!(kzg_multipoly::verify::<E, P>(block, &verifier_key));
        }

        // from [`FECBLock`], the start of the data is after:
        //     u32 (4 bytes) +
        //     u32 (4 bytes) +
        //     vector length (8 bytes) +
        //     hash (32 bytes) +
        //     vector length (8 bytes)
        let corrupted_block = corrupt_block::<E>(blocks.get(0).unwrap(), 4 + 4 + 8 + 32 + 8, 1);

        assert!(!kzg_multipoly::verify::<E, P>(
            &corrupted_block,
            &verifier_key
        ));

        // let's build some blocks containing errors
        let mut blocks_with_errors = Vec::new();

        let b3 = blocks.get(3).unwrap();
        blocks_with_errors.push(KZGMultipolyBlock {
            shard: b3.shard.clone(),
            commit: b3.commit.clone(),
            proof: b3.proof,
        });
        assert!(kzg_multipoly::batch_verify(blocks_with_errors.as_slice(), &verifier_key).unwrap());

        blocks_with_errors.push(corrupted_block);
        assert!(
            !kzg_multipoly::batch_verify(blocks_with_errors.as_slice(), &verifier_key).unwrap()
        );

        Ok(())
    }
    */

    fn reconstruct_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
        l: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        assert!(
            n >= k + l,
            "kzg_multipoly::tests::reconstruct_template requires at least {} = k (= {}) + l (= {}) shards, found {}",
            k + l,
            k,
            l,
            n
        );

        let (blocks, _) = test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        let mut shards = blocks.iter().map(|x| x.shard.clone()).collect::<Vec<_>>();

        for _ in 0..l {
            shards.remove(0);
        }

        assert_eq!(bytes.to_vec(), decode::<GF>(shards).unwrap());

        Ok(())
    }

    #[test]
    fn verify_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        verify_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes, 4, 6, 2)
            .expect("verification failed for bls12-381");
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    /*
    #[test]
    fn verify_with_errors_2() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_with_errors_4() {
        let bytes = bytes::<Bls12_381>(4, 4);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn verify_with_errors_6() {
        let bytes = bytes::<Bls12_381>(4, 6);
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes, 4, 6)
            .expect("verification failed for bls12-381");
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }
    */

    #[test]
    fn pc_interface() {
        let bytes = bytes::<Bls12_381>(8, 2);
        let degree =
            bytes.len() / (<Bls12_381 as Pairing>::ScalarField::MODULUS_BIT_SIZE as usize / 8);

        let rng = &mut test_rng();

        let params = KZG10::<Bls12_381, UniPoly381>::setup(degree, false, rng).unwrap();
        let (powers, verifier_key) = trim(params, degree);

        let elements = field::split_data_into_field_elements::<Bls12_381>(&bytes, 8);
        let nb_polynomials = elements.len() / 8;
        let polynomials = field::build_interleaved_polynomials::<Bls12_381, UniPoly381>(
            &elements,
            nb_polynomials,
        );

        let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
        let points: Vec<<Bls12_381 as Pairing>::ScalarField> = (0..16)
            .map(|i| <Bls12_381 as Pairing>::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        let commit =
            KzgMultiCommitment::<Bls12_381, UniPoly381>::commit(data.clone(), Some(powers.clone()))
                .unwrap();

        // no setup given: Should return an error
        let _ = KzgMultiCommitment::<Bls12_381, UniPoly381>::prove(
            commit.clone(),
            data.clone(),
            None,
            None,
        )
        .unwrap_err();

        let blocks = KzgMultiCommitment::<Bls12_381, UniPoly381>::prove(
            commit,
            data,
            Some(&points),
            Some(powers),
        )
        .unwrap();

        // no errors, should have N blocks
        assert_eq!(blocks.len(), 16);

        for block in &blocks {
            assert!(KzgMultiCommitment::<Bls12_381, UniPoly381>::verify(
                block,
                Some(verifier_key.clone())
            )
            .unwrap());
        }

        assert!(KzgMultiCommitment::<Bls12_381, UniPoly381>::batch_verify(
            &blocks[1..3],
            Some(verifier_key),
        )
        .unwrap());
    }
}
