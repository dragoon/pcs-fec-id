//! aPlonK: an extension of KZG+ where commits are _folded_ into one
//!
//! > references:
//! > - [Ambrona et al., 2022](https://link.springer.com/chapter/10.1007/978-3-031-41326-1_11)
use ark_ec::{
    pairing::{Pairing, PairingOutput},
    AffineRepr,
};
use ark_ff::{BigInteger, Field, PrimeField};
use ark_poly::DenseUVPolynomial;
use ark_poly_commit::{
    kzg10::{Proof, Randomness, UniversalParams, VerifierKey, KZG10},
    PCRandomness,
};
use ark_serialize::{CanonicalDeserialize, CanonicalSerialize, Compress};
use ark_std::{test_rng, One, UniformRand};
use fec::Shard;
use rs_merkle::algorithms::Sha256;
use rs_merkle::Hasher;
use std::marker::PhantomData;
use std::ops::{Div, Mul};

use crate::{kzg_multipoly, Error, PolynomialCommitmentScheme};
use crate::{trim, u32_to_u8_vec};

pub(super) mod algebra;
mod ipa;
mod polynomial;
mod transcript;

#[derive(Debug, Clone, Default, PartialEq, CanonicalSerialize, CanonicalDeserialize)]
pub struct AplonkBlock<E: Pairing> {
    pub shard: Shard,
    com_f: PairingOutput<E>,
    v_hat: E::ScalarField,
    mu_hat: E::G1,
    kzg_proof: Proof<E>,
    ipa_proof: ipa::IpaProof<E>,
    aplonk_proof: E::G2,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct AplonkCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    _engine: PhantomData<E>,
    _poly: PhantomData<P>,
}

impl<E, P> PolynomialCommitmentScheme for AplonkCommitment<E, P>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    type Data = ([u8; 32], usize, Vec<P>);
    type Point = E::ScalarField;
    type Commit = (Vec<E::G1>, PairingOutput<E>);
    type Proof = AplonkBlock<E>;
    type Setup<'a> = SetupParams<E>;
    type VerifierKey<'b> = AplonkVerifierKey<E>;

    fn commit(data: Self::Data, setup: Option<Self::Setup<'_>>) -> Result<Self::Commit, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (_, _, polynomials) = data;

        commit(polynomials, setup)
    }

    fn prove(
        commit: Self::Commit,
        data: Self::Data,
        points: Option<&[Self::Point]>,
        setup: Option<Self::Setup<'_>>,
    ) -> Result<Vec<Self::Proof>, Error> {
        let setup = if let Some(s) = setup {
            s
        } else {
            return Err(Error::SetupError);
        };

        let (hash, nb_bytes, polynomials) = data;

        let k = polynomials[0].coeffs().len();

        let shards = if let Some(pts) = points {
            pts.iter()
                .enumerate()
                .map(|(i, point)| {
                    let shard = polynomials
                        .iter()
                        .flat_map(|p| p.evaluate(point).into_bigint().to_bytes_le())
                        .collect::<Vec<u8>>();

                    Shard {
                        k: k as u32,
                        i: i as u32,
                        hash: hash.to_vec(),
                        bytes: shard,
                        size: nb_bytes,
                    }
                })
                .collect()
        } else {
            return Err(Error::ProofPointMissingError);
        };

        prove::<E, P>(commit, polynomials, shards, setup)
    }

    fn verify(
        proof: &Self::Proof,
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        verify(
            proof,
            &verifier_key.vk_psi,
            verifier_key.tau_1,
            verifier_key.g1,
            verifier_key.g2,
        )
    }

    fn batch_verify(
        proofs: &[Self::Proof],
        verifier_key: Option<Self::VerifierKey<'_>>,
    ) -> Result<bool, Error> {
        let verifier_key = if let Some(vk) = verifier_key {
            vk
        } else {
            return Err(Error::VerifierKeyError);
        };

        for proof in proofs {
            match verify(
                proof,
                &verifier_key.vk_psi,
                verifier_key.tau_1,
                verifier_key.g1,
                verifier_key.g2,
            ) {
                Ok(result) => {
                    if !result {
                        return Ok(false);
                    }
                }
                Err(error) => return Err(error),
            }
        }

        Ok(true)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct SetupParams<E: Pairing> {
    pub kzg: UniversalParams<E>,
    pub ipa: ipa::IPAParams<E>,
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct AplonkVerifierKey<E: Pairing> {
    pub vk_psi: VerifierKey<E>,
    pub tau_1: E::G1,
    pub g1: E::G1,
    pub g2: E::G2,
}

/// creates a combination of a trusted KZG and an IPA setup for [[aPlonk]]
///
/// > **Note**  
/// > this is an almost perfect translation of the *Setup* algorithm in page
/// > **13** of [aPlonk from [Ambrona et al.]][aPlonK]
///
/// [aPlonk]: https://eprint.iacr.org/2022/1352.pdf
pub fn setup<E, P>(
    degree_bound: usize,
    nb_polynomials: usize,
) -> Result<SetupParams<E>, ark_poly_commit::Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let rng = &mut test_rng();
    let params = KZG10::<E, P>::setup(degree_bound, true, rng)?;

    let g_1 = params.powers_of_g[0];
    let g_2 = params.h;

    let tau = E::ScalarField::rand(rng);
    let ck_tau = algebra::powers_of::<E>(tau, nb_polynomials)
        .iter()
        .map(|t| g_2.mul(t))
        .collect();

    Ok(SetupParams {
        kzg: params,
        ipa: ipa::IPAParams {
            ck_tau,
            tau_1: g_1.mul(tau),
        },
    })
}

fn commit<E, P>(
    polynomials: Vec<P>,
    setup: SetupParams<E>,
) -> Result<(Vec<E::G1>, PairingOutput<E>), Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let supported_degree = polynomials.iter().map(|p| p.degree()).max().unwrap_or(0);

    if setup.ipa.ck_tau.len() < polynomials.len() {
        return Err(Error::SetupError);
    }

    let (powers, _) = trim(setup.kzg, supported_degree);

    if powers.powers_of_g.len() <= supported_degree {
        return Err(Error::SetupError);
    }

    // commit.1.
    let mu = match kzg_multipoly::commit(&powers, &polynomials) {
        Ok((mu, _)) => mu,
        Err(error) => return Err(Error::ArkError(error)),
    };
    let mu: Vec<E::G1> = mu.iter().map(|c| c.0.into_group()).collect();

    // commit.2.
    let com_f: PairingOutput<E> = mu
        .iter()
        .enumerate()
        .map(|(i, c)| E::pairing(c, setup.ipa.ck_tau[i]))
        .sum();

    Ok((mu, com_f))
}

fn prove<E, P>(
    commit: (Vec<E::G1>, PairingOutput<E>),
    polynomials: Vec<P>,
    shards: Vec<Shard>,
    params: SetupParams<E>,
) -> Result<Vec<AplonkBlock<E>>, Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let (mu, com_f) = commit;

    let supported_degree = polynomials.iter().map(|p| p.degree()).max().unwrap_or(0);
    let (powers, _) = trim(params.kzg, supported_degree);

    // open
    let mut proofs = Vec::new();
    for shard in shards {
        let point = E::ScalarField::from_le_bytes_mod_order(&[shard.i as u8]);

        let v_hat_elements = polynomials
            .iter()
            .map(|p| p.evaluate(&point))
            .collect::<Vec<E::ScalarField>>();

        // open.3.1.
        let mut r_bytes = vec![];
        if let Err(error) = com_f.serialize_with_mode(&mut r_bytes, Compress::Yes) {
            return Err(Error::SerializationError(error));
        }
        if let Err(error) = point.serialize_with_mode(&mut r_bytes, Compress::Yes) {
            return Err(Error::SerializationError(error));
        }
        // FIXME: hash *com_v* here
        let hash = Sha256::hash(r_bytes.as_slice());
        let r = E::ScalarField::from_le_bytes_mod_order(&hash);

        // open.3.2.
        let r_vec = algebra::powers_of::<E>(r, polynomials.len());
        // open.3.3
        let f = algebra::scalar_product_polynomial::<E, P>(&r_vec, &polynomials);
        // open.3.4.
        let mu_hat: E::G1 = algebra::scalar_product_g1::<E>(&mu, &r_vec);
        // open.3.5.
        let v_hat: E::ScalarField = algebra::scalar_product::<E>(&v_hat_elements, &r_vec);

        // open.4.
        let kzg_proof = match KZG10::<E, P>::open(
            &powers,
            &f,
            point,
            &Randomness::<E::ScalarField, P>::empty(),
        ) {
            Ok(proof) => proof,
            Err(error) => return Err(Error::ArkError(error)),
        };

        // open.5.
        // we do no need this step as we already share the shards on the network

        // open.6.
        let (ipa_proof, u) =
            match ipa::prove(polynomials.len(), &params.ipa.ck_tau, com_f, r, mu_hat, &mu) {
                Ok(proof) => proof,
                Err(error) => return Err(error),
            };
        let mut u_inv = Vec::new();
        for u_i in &u {
            if let Some(inverse) = u_i.inverse() {
                u_inv.push(inverse)
            } else {
                return Err(Error::EllipticInverseError);
            }
        }

        // open.7.1.
        let kappa = f64::log2(polynomials.len() as f64) as usize;
        let g = polynomial::compute_g::<E, P>(polynomials.len(), kappa, &u, &u_inv);
        // open.7.2.
        let mut rho_bytes = vec![];
        if let Err(error) = ipa_proof.serialize_with_mode(&mut rho_bytes, Compress::Yes) {
            return Err(Error::SerializationError(error));
        }
        let rho = E::ScalarField::from_le_bytes_mod_order(&Sha256::hash(rho_bytes.as_slice()));
        // open.7.3.
        // implicit in the computation of the witness polynomial

        // open.8.1.
        let h = match KZG10::<E, P>::compute_witness_polynomial(
            &g,
            rho,
            &Randomness::<E::ScalarField, P>::empty(),
        ) {
            Ok((h, _)) => h,
            Err(error) => return Err(Error::ArkError(error)),
        };
        // open.8.2.
        let aplonk_proof = h
            .coeffs()
            .iter()
            .enumerate()
            .map(|(i, hi)| params.ipa.ck_tau[i].mul(hi))
            .sum();

        // open.9.
        proofs.push(AplonkBlock {
            shard,
            com_f,
            v_hat,
            mu_hat,
            kzg_proof,
            ipa_proof,
            aplonk_proof,
        });
    }

    Ok(proofs)
}

fn verify<E, P>(
    block: &AplonkBlock<E>,
    vk_psi: &VerifierKey<E>,
    tau_1: E::G1,
    g_1: E::G1,
    g_2: E::G2,
) -> Result<bool, Error>
where
    E: Pairing,
    P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
    for<'a, 'b> &'a P: Div<&'b P, Output = P>,
{
    let point = E::ScalarField::from_le_bytes_mod_order(&u32_to_u8_vec(block.shard.i));

    // check.1.
    let mut bytes = vec![];
    if let Err(error) = block.com_f.serialize_with_mode(&mut bytes, Compress::Yes) {
        return Err(Error::SerializationError(error));
    }
    if let Err(error) = point.serialize_with_mode(&mut bytes, Compress::Yes) {
        return Err(Error::SerializationError(error));
    }
    // FIXME: hash *com_v* here
    let hash = Sha256::hash(bytes.as_slice());
    let r = E::ScalarField::from_le_bytes_mod_order(&hash);

    // check.2.
    let p1 = block.mu_hat - vk_psi.g.mul(block.v_hat);
    let inner = vk_psi.beta_h.into_group() - vk_psi.h.mul(&point);
    if E::pairing(p1, vk_psi.h) != E::pairing(block.kzg_proof.w, inner) {
        return Ok(false);
    }

    // TODO: missing part of the aplonk algorithm
    // check.3.

    let nb_polynomials = block.shard.size
        / (block.shard.k as usize)
        / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);

    // check.4.
    if !ipa::verify(
        nb_polynomials,
        None, // we call *IPA.Verify'* here
        block.com_f,
        r,
        block.mu_hat,
        &block.ipa_proof,
    )? {
        return Ok(false);
    }

    // check.5.1.
    let mut bytes = vec![];
    if let Err(error) = block
        .ipa_proof
        .serialize_with_mode(&mut bytes, Compress::Yes)
    {
        return Err(Error::SerializationError(error));
    }
    let hash = Sha256::hash(bytes.as_slice());
    let rho = E::ScalarField::from_le_bytes_mod_order(&hash);

    let kappa = f64::log2(nb_polynomials as f64) as usize;
    let mut ts = match transcript::initialize(block.com_f, r, block.mu_hat) {
        Ok(transcript) => transcript,
        Err(error) => return Err(Error::SerializationError(error)),
    };
    let mut u = algebra::vector::zero::<E::ScalarField>(kappa);
    for j in (0..kappa).rev() {
        u[j] = match transcript::hash(
            block.ipa_proof.l_g[j],
            block.ipa_proof.r_g[j],
            block.ipa_proof.l_r[j],
            block.ipa_proof.r_r[j],
            &ts,
        ) {
            Ok(hash) => hash,
            Err(error) => return Err(Error::SerializationError(error)),
        };
        ts = match transcript::reset::<E>(u[j]) {
            Ok(transcript) => transcript,
            Err(error) => return Err(Error::SerializationError(error)),
        };
    }

    let mut u_inv = Vec::new();
    for u_i in &u {
        if let Some(inverse) = u_i.inverse() {
            u_inv.push(inverse)
        } else {
            return Err(Error::EllipticInverseError);
        }
    }

    // check.5.2.
    let g = polynomial::compute_g::<E, P>(nb_polynomials, kappa, &u, &u_inv);
    let v_rho = g.evaluate(&rho);

    // check.6.
    let lhs = E::pairing(tau_1 - g_1.mul(rho), block.aplonk_proof);
    let rhs = E::pairing(
        g_1.mul(E::ScalarField::one()),
        block.ipa_proof.ck_tau_0 - g_2.mul(v_rho),
    );
    let b_tau = lhs == rhs;

    // check.7.
    // the formula is implicit because here
    //     - b_psi has passed in check.2.
    //     - b_v is skipped for now
    //     - b_IPA has passed in check.4.
    Ok(b_tau)
}

#[cfg(test)]
mod tests {
    use crate::aplonk_pc::{self, prove, setup, AplonkBlock, AplonkCommitment, AplonkVerifierKey};
    use crate::{field, trim, PolynomialCommitmentScheme};

    use ark_bls12_381::Bls12_381;
    use ark_ec::{pairing::Pairing, AffineRepr};
    use ark_ff::{BigInteger, Field, PrimeField};
    use ark_poly::{univariate::DensePolynomial, DenseUVPolynomial};
    use ark_poly_commit::kzg10::VerifierKey;
    use fec::{decode, Shard};
    use reed_solomon_erasure::galois_prime::Field as GF;
    use rs_merkle::{algorithms::Sha256, Hasher};
    use std::ops::{Div, MulAssign};

    use super::commit;

    type UniPoly381 = DensePolynomial<<Bls12_381 as Pairing>::ScalarField>;

    fn bytes<E: Pairing>(k: usize, nb_polynomials: usize) -> Vec<u8> {
        let nb_bytes = k * nb_polynomials * (E::ScalarField::MODULUS_BIT_SIZE as usize / 8);
        include_bytes!("../../tests/dragoon_133x133.png")[0..nb_bytes].to_vec()
    }

    #[allow(clippy::type_complexity)]
    fn test_setup<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<
        (Vec<AplonkBlock<E>>, (VerifierKey<E>, E::G1), (E::G1, E::G2)),
        ark_poly_commit::Error,
    >
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let degree = k - 1;
        let vector_length_bound =
            bytes.len() / (E::ScalarField::MODULUS_BIT_SIZE as usize / 8) / (degree + 1);

        let params = setup::<E, P>(degree, vector_length_bound)?;
        let (_, vk_psi) = trim(params.kzg.clone(), degree);

        let elements = field::split_data_into_field_elements::<E>(bytes, k);

        let nb_polynomials = elements.len() / k;
        let polynomials = field::build_interleaved_polynomials::<E, P>(&elements, nb_polynomials);

        let commit = commit(polynomials.clone(), params.clone()).unwrap();

        let hash = Sha256::hash(bytes);
        let shards = (0..n)
            .map(|i| {
                let point = E::ScalarField::from_le_bytes_mod_order(&[i as u8]);

                let shard = polynomials
                    .iter()
                    .flat_map(|p| p.evaluate(&point).into_bigint().to_bytes_le())
                    .collect::<Vec<u8>>();

                Shard {
                    k: k as u32,
                    i: i as u32,
                    hash: hash.to_vec(),
                    bytes: shard,
                    size: bytes.len(),
                }
            })
            .collect();

        let blocks = prove::<E, P>(commit, polynomials, shards, params.clone()).unwrap();

        Ok((
            blocks,
            (vk_psi, params.ipa.tau_1),
            (
                params.kzg.powers_of_g[0].into_group(),
                params.kzg.h.into_group(),
            ),
        ))
    }

    fn verify_template<E, P>(bytes: &[u8], k: usize, n: usize) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, (vk_psi, tau_1), (g_1, g_2)) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in blocks {
            assert!(aplonk_pc::verify::<E, P>(&block, &vk_psi, tau_1, g_1, g_2).unwrap());
        }

        Ok(())
    }

    fn verify_with_errors_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        let (blocks, (vk_psi, tau_1), (g_1, g_2)) =
            test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        for block in blocks {
            let mut b = block.clone();
            // modify a field in the struct b to corrupt the block proof without corrupting the data serialization
            let a = E::ScalarField::from_le_bytes_mod_order(&[123]);
            b.ipa_proof.l_r[0].mul_assign(a.pow([4321_u64]));

            assert!(
                !aplonk_pc::verify::<E, P>(&b, &vk_psi, tau_1, g_1, g_2).unwrap(),
                "aPlonK should fail for bls12-381, k = {} and a corrupted block",
                k
            );
        }

        Ok(())
    }

    fn reconstruct_template<E, P>(
        bytes: &[u8],
        k: usize,
        n: usize,
        l: usize,
    ) -> Result<(), ark_poly_commit::Error>
    where
        E: Pairing,
        P: DenseUVPolynomial<E::ScalarField, Point = E::ScalarField>,
        for<'a, 'b> &'a P: Div<&'b P, Output = P>,
    {
        assert!(
            n >= k + l,
            "kzg_multipoly::tests::reconstruct_template requires at least {} = k (= {}) + l (= {}) shards, found {}",
            k + l,
            k,
            l,
            n
        );

        let (blocks, _, _) = test_setup::<E, P>(bytes, k, n).expect("proof failed for bls12-381");

        let mut shards = blocks.iter().map(|x| x.shard.clone()).collect::<Vec<_>>();

        for _ in 0..l {
            shards.remove(0);
        }

        assert_eq!(bytes.to_vec(), decode::<GF>(shards).unwrap());

        Ok(())
    }

    #[test]
    fn verify_2() {
        verify_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 2), 4, 6)
            .expect("verification failed for bls12-381");
    }

    #[test]
    fn verify_4() {
        verify_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 4), 4, 6)
            .expect("verification failed for bls12-381");
    }

    #[test]
    fn verify_8() {
        verify_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 8), 4, 6)
            .expect("verification failed for bls12-381");
    }

    #[test]
    fn verify_with_errors_2() {
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 2), 4, 6)
            .expect("verification failed for bls12-381");
    }

    #[test]
    fn verify_with_errors_4() {
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 4), 4, 6)
            .expect("verification failed for bls12-381");
    }

    #[test]
    fn verify_with_errors_8() {
        verify_with_errors_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 8), 4, 6)
            .expect("verification failed for bls12-381");
    }

    // TODO: implement padding for aPlonK
    #[ignore = "padding not supported by aPlonK"]
    #[test]
    fn verify_with_padding_test() {
        let bytes = bytes::<Bls12_381>(4, 2);
        verify_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn reconstruct_2() {
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 2), 4, 6, 2)
            .expect("reconstruction failed for bls12-381");
    }

    #[test]
    fn reconstruct_4() {
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 4), 4, 6, 2)
            .expect("reconstruction failed for bls12-381");
    }

    #[test]
    fn reconstruct_8() {
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes::<Bls12_381>(4, 8), 4, 6, 2)
            .expect("reconstruction failed for bls12-381");
    }

    // TODO: implement padding for aPlonK
    #[ignore = "padding not supported by aPlonK"]
    #[test]
    fn reconstruct_with_padding() {
        let bytes = bytes::<Bls12_381>(4, 2);
        reconstruct_template::<Bls12_381, UniPoly381>(&bytes[0..(bytes.len() - 33)], 4, 6, 2)
            .expect("verification failed for bls12-381 with padding");
    }

    #[test]
    fn pc_interface() {
        let bytes = bytes::<Bls12_381>(4, 2);
        let degree = 4 - 1;
        let vector_length_bound = bytes.len()
            / (<Bls12_381 as Pairing>::ScalarField::MODULUS_BIT_SIZE as usize / 8)
            / (degree + 1);

        let params = setup::<Bls12_381, UniPoly381>(degree, vector_length_bound).unwrap();
        let (_, vk_psi) = trim(params.kzg.clone(), degree);

        let elements = field::split_data_into_field_elements::<Bls12_381>(&bytes, 4);
        let nb_polynomials = elements.len() / 4;
        let polynomials = field::build_interleaved_polynomials::<Bls12_381, UniPoly381>(
            &elements,
            nb_polynomials,
        );

        let data = (Sha256::hash(&bytes), bytes.len(), polynomials);
        let points: Vec<<Bls12_381 as Pairing>::ScalarField> = (0..6)
            .map(|i| <Bls12_381 as Pairing>::ScalarField::from_le_bytes_mod_order(&[i as u8]))
            .collect();

        let commit =
            AplonkCommitment::<Bls12_381, UniPoly381>::commit(data.clone(), Some(params.clone()))
                .unwrap();

        // no setup given: Should return an error
        let _ = AplonkCommitment::<Bls12_381, UniPoly381>::prove(
            commit.clone(),
            data.clone(),
            None,
            None,
        )
        .unwrap_err();

        let blocks = AplonkCommitment::<Bls12_381, UniPoly381>::prove(
            commit,
            data,
            Some(&points),
            Some(params.clone()),
        )
        .unwrap();

        let vk = AplonkVerifierKey {
            vk_psi,
            tau_1: params.ipa.tau_1,
            g1: params.kzg.powers_of_g[0].into_group(),
            g2: params.kzg.h.into_group(),
        };

        // no errors, should have N blocks
        assert_eq!(blocks.len(), 6);

        for block in &blocks {
            assert!(
                AplonkCommitment::<Bls12_381, UniPoly381>::verify(block, Some(vk.clone())).unwrap()
            );
        }

        assert!(
            AplonkCommitment::<Bls12_381, UniPoly381>::batch_verify(&blocks[1..3], Some(vk),)
                .unwrap()
        );
    }
}
